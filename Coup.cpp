#include "Coup.h"

Coup::Coup(int coord_x, int coord_y, int dir_x, int dir_y, int distance)
{
    m_coord_x=coord_x;
    m_coord_y=coord_y;

    for(int i=0; i<m_size_plateau; i++)
    {
        std::vector<bool> ligne;
        for(int j=0; j<m_size_plateau; j++)
            ligne.push_back(false);

        m_changement.push_back(ligne);
    }

    for(int advance=0; advance<distance; advance++)
    {
        coord_x=m_coord_x+advance*dir_x;
        coord_y=m_coord_y+advance*dir_y;
        m_changement[coord_y][coord_x]=true;
    }
}

Coup::~Coup()
{

}

void Coup::update_possibilities(int dir_x, int dir_y, int distance)
{
    int coord_x=m_coord_x;
    int coord_y=m_coord_y;

    for(int advance=0; advance<distance; advance++)
    {
        coord_x=m_coord_x+advance*dir_x;
        coord_y=m_coord_y+advance*dir_y;
        m_changement[coord_y][coord_x]=true;
    }
}

int Coup::get_coord_x()
{
    return m_coord_x;
}

int Coup::get_coord_y()
{
    return m_coord_y;
}

int Coup::get_weight()
{
    return m_weight;
}

void Coup::set_weight()
{
    for(int i=0; i<m_size_plateau; i++)
        for(int j=0; j<m_size_plateau; j++)
            if(m_changement[i][j])
                m_weight++;
}

bool Coup::get_part_of_change(int x, int y)
{
    return m_changement[y][x];
}
/*
Coup* Coup::get_ancetre()
{
    return m_ancetre;
}

void Coup::set_ancetre(Coup* ancetre)
{
    m_ancetre=ancetre;
}

Coup* Coup::get_one_descendant(unsigned int n)
{
    return m_descendants[n];
}

void Coup::set_one_descendant(Coup* descendant)
{
    m_descendants.push_back(descendant);
}
*/
