///En gros on dit qu'un coup, c'est une matrice qui dit quels sont les pions chang� lorsqu'un pion est plac� � tel endroit...
#include<vector>
#include <iostream>

class Coup
{
private:
    int m_coord_x;
    int m_coord_y;
    int m_size_plateau=8;///Franchement inutile � part si on veut changer la taille du plateau, ce que personne de sain d'esprit ne ferait
    int m_weight=0;

    std::vector<std::vector<bool>> m_changement;

    ///Ce que je m'appr�te � coder ressemble fortement � une liste, ne reproduisez pas �a chez vous si vous tenez � votre sant� mentale
    ///Si vous n'y tenez pas: https:///www.youtube.com/watch?v=dQw4w9WgXcQ&list=PL5RPJEeBZVzXQIT3JOi0b-EQ6237FfpqK&index=64
    /*
    std::vector<Coup*> m_descendants;
    Coup* m_ancetre=NULL;
    */

public:
    Coup(int coord_x, int coord_y,int dir_x, int dir_y, int distance);
    ~Coup();

    void update_possibilities(int dir_x, int dir_y, int distance);

    int get_coord_x();
    int get_coord_y();

    int get_weight();
    void set_weight();

    bool get_part_of_change(int x, int y);
/*
    Coup* get_ancetre();
    void set_ancetre(Coup* ancetre);

    Coup* get_one_descendant(unsigned int n);
    void set_one_descendant(Coup* descendant);
*/
};
