#include "Othello.h"
#include "console.h"
#include "windows.h"

Othello::Othello()
{
    for(int i=0; i<m_size; i++)
    {
        std::vector<Pion> ligne;
        for(int j=0; j<m_size; j++)
            ligne.push_back(Pion(j,i,-1));

        m_othellier.push_back(ligne);
    }
}

Othello::~Othello()
{
    for(int i=0; i<m_size; i++)
        for(int j=0; j<m_size; j++)
            m_othellier[i].pop_back();
}

void Othello::initiate_othellier()
{
    ///On initialise le tableau tel qu'il doit �tre en d�but de partie, avec les 2 pions de chaque camps
    m_nb_pions=4;

    m_othellier[3][3].set_side(1);
    m_othellier[4][3].set_side(0);
    m_othellier[3][4].set_side(0);
    m_othellier[4][4].set_side(1);

    set_adjacence(3,3,1);
    set_adjacence(4,3,0);
    set_adjacence(3,4,0);
    set_adjacence(4,4,1);
}

Console* Othello::get_console()
{
    return m_console;
}

void Othello::launch_menu()
{
    bool Echap_menu=false;
    std::string choix="";///Variable interm�diaire de lecture du buffer, pour �viter les overflow si l'utilisateur rentre n'importe quoi
    int choice=0;///vrai choix de l'utilisateur
    char point=char(254);
    int posx=6;
    int posy=3;
    int touche=0;
    int cursor_x=posx;
    int cursor_y;


    while(!Echap_menu)
    {
        system("cls");
        fflush(stdin);

        std::cout<<"\t\t ------------------------"<<std::endl;
        std::cout<<"\t\t|\t\t\t |"<<std::endl;
        std::cout<<"\t\t|\t OTHELLO\t |"<<std::endl;
        std::cout<<"\t\t|\t\t\t |"<<std::endl;
        std::cout<<"\t\t*------------------------*"<<std::endl<<std::endl;

        m_console->_setColor(COLOR_LIGHT_GREEN,COLOR_BLACK);
        std::cout<< "   " << point;
        m_console->_setColor(COLOR_WHITE,COLOR_BLACK);
        std::cout<<" 1"<< "\t"<<" Lancer une partie en 1 vs 1"<<std::endl;
        m_console->_setColor(COLOR_LIGHT_GREEN,COLOR_BLACK);
        std::cout<< "   " << point;
        m_console->_setColor(COLOR_WHITE,COLOR_BLACK);
        std::cout<<" 2"<<"\t"<<" Lancer une partie en mode facile"<<std::endl;
        m_console->_setColor(COLOR_LIGHT_GREEN,COLOR_BLACK);
        std::cout<< "   " << point;
        m_console->_setColor(COLOR_WHITE,COLOR_BLACK);
        std::cout<<" 3"<<"\t"<<" Lancer une partie en mode intermediaire"<<std::endl<<std::endl;
        m_console->_setColor(COLOR_RED,COLOR_BLACK);
        std::cout<< "   " << point;
        m_console->_setColor(COLOR_WHITE,COLOR_BLACK);
        std::cout<<" 4"<<"\t"<<" Reprendre une sauvegarde en 1 vs 1"<<std::endl;
        m_console->_setColor(COLOR_RED,COLOR_BLACK);
        std::cout<< "   " << point;
        m_console->_setColor(COLOR_WHITE,COLOR_BLACK);
        std::cout<<" 5"<<"\t"<<" Reprendre une partie en mode facile"<<std::endl;
        m_console->_setColor(COLOR_RED,COLOR_BLACK);
        std::cout<< "   " << point;
        m_console->_setColor(COLOR_WHITE,COLOR_BLACK);
        std::cout<<" 6"<<"\t"<<" Reprendre une partie en mode intermediaire"<<std::endl<<std::endl;
        m_console->_setColor(COLOR_TURQUOISE,COLOR_BLACK);


        std::cout<< "   " << point;
        m_console->_setColor(COLOR_WHITE,COLOR_BLACK);
        std::cout<<" 7"<<"\t"<<" Quiter le jeu"<<std::endl;

        m_console->gotoLigCol(posx,posy);

        if (m_console->getInputKey())
        {
            touche = m_console->getInputKey();


            switch(touche)
            {
            case 'z':
                posx=posx-1;
                cursor_x=posx;
                cursor_y=posy;
                m_console->gotoLigCol(posx,posy);
                if (posx > 14) posx=6;
                if (posx < 6) posx = 14;
                break;

            case 's':
                posx=posx+1;
                cursor_x=posx;
                cursor_y=posy;
                m_console->gotoLigCol(posx,posy);
                if (posx > 14) posx=6;
                if (posx < 6) posx = 14;
                break;

            default:
                break;
            }
        }

        if (cursor_x==6 && cursor_y==3)
        {
            m_console->gotoLigCol(cursor_x,cursor_y);
            m_console->_setColor(20,0);
        }
        else if (cursor_x==7 && cursor_y==3)
        {
            m_console->gotoLigCol(cursor_x,cursor_y);
            m_console->_setColor(20,0);
        }
        else if (cursor_x==8 && cursor_y==3)
        {
            m_console->gotoLigCol(cursor_x,cursor_y);
            m_console->_setColor(20,0);
        }
        else if (cursor_x==10 && cursor_y==3)
        {
            m_console->gotoLigCol(cursor_x,cursor_y);
            m_console->_setColor(20,0);
        }
        else if (cursor_x==11 && cursor_y==3)
        {
            m_console->gotoLigCol(cursor_x,cursor_y);
            m_console->_setColor(20,0);
        }
        else if (cursor_x==12 && cursor_y==3)
        {
            m_console->gotoLigCol(cursor_x,cursor_y);
            m_console->_setColor(20,0);
        }
        else if (cursor_x==14 && cursor_y==3)
        {
            m_console->gotoLigCol(cursor_x,cursor_y);
            m_console->_setColor(20,0);
        }

        if(m_console->getInputKey()!='z' && m_console->getInputKey()!='s' && m_console->getInputKey()!=13)
        {
            choix=m_console->getInputKey();
            choice=int(choix[0])-48;
        }

        m_console->_setColor(COLOR_WHITE,COLOR_BLACK);
        if((m_console->getInputKey()==13 && posx==6) || choice==1)
        {
            initiate_othellier();
            game();
        }
        else if ((m_console->getInputKey()==13 && posx==7) || choice==2)
        {
            m_beginner_AI=true;
            initiate_othellier();
            side_choice();
            game();
        }
        else if ((m_console->getInputKey()==13 && posx==8) || choice==3)
        {
            m_intermediary_AI=true;
            initiate_othellier();
            side_choice();
            game();
        }
        else if ((m_console->getInputKey()==13 && posx==10) || choice==4)
        {
            get_save(m_save_file);
            game();
        }
        else if ((m_console->getInputKey()==13 && posx==11) || choice==5)
        {
            m_beginner_AI=true;
            get_save(m_save_file_beginner_AI);
            game();
        }
        else if ((m_console->getInputKey()==13 && posx==12) || choice==6)
        {
            m_intermediary_AI=true;
            get_save(m_save_file_intermediary_AI);
            game();
        }
        else if ((m_console->getInputKey()==13 && posx==14) || choice==7)
        {
            Echap_menu=true;
        }
    }
}

void Othello::side_choice()
{
    std::string choix="";///Variable interm�diaire de lecture du buffer, pour �viter les overflow si l'utilisateur rentre n'importe quoi
    int choice=0;///vrai choix de l'utilisateur
    bool valid_choice=false;

    while(!valid_choice)
    {
        system("cls");
        fflush(stdin);

        std::cout<<"Quelle couleur voulez vous jouer?"<<std::endl<<std::endl;

        std::cout<<"1. Blanc"<<std::endl;
        std::cout<<"2. Noir"<<std::endl;

        std::cin>>choix;
        choice=int(choix[0])-48;

        switch(choice)
        {
        case 1:
            m_player_white=true;
            valid_choice=true;
            break;

        case 2:
            m_player_white=false;
            valid_choice=true;
            break;
        }
    }
}

void Othello::get_save(std::string file_name)///Ici on ne fait que mettre � jour le terrain de jeu en fonction de la sauvegarde, on cr�er le terrain et on choisit le mode de jeu en dehors
{
    std::ifstream file(file_name, std::ios::in);
    int read_side;

    if(file)
    {
        m_nb_pions=0;///On s'assure que l'on part de 0 pions

        file>>m_turn_white;///On d�termine le tour en cours, avec un 0 ou un 1 dans le fichier
        if(m_beginner_AI || m_intermediary_AI)
            file>>m_player_white;///On d�termine si le joueur controle les pions blancs ou noirs

        for(int i=0; i<m_size; i++)
            for(int j=0; j<m_size; j++)
            {
                file>>read_side;
                if(read_side!=-1)///On compte le nouveau pion
                    m_nb_pions++;

                m_othellier[i][j].set_side(read_side);
                set_adjacence(j,i,read_side);
            }
        file.close();
    }
}

void Othello::create_save(std::string file_name)
{
    std::ofstream file(file_name, std::ios::out | std::ios::trunc);

    if(file)
    {
        file<<int(m_turn_white);

        if(m_player_white && (m_beginner_AI || m_intermediary_AI))
            file<<1;
        else if(m_beginner_AI || m_intermediary_AI)
            file<<0;

        for(int i=0; i<m_size; i++)
            for(int j=0; j<m_size; j++)
            {
                file<<m_othellier[i][j].get_side();
                file<<" ";
            }
        file.close();
    }
}

void Othello::move_choice()
{
    bool valid_entry=false;

    int Enter_bouton=13;
    int Echap_bouton=27;

    fflush(stdin);
    while(!valid_entry && !m_Echap)
    {
        show_game();
        ///Pour tester, � la place de montrer le jeu on va montrer le tableau d'ajacence de CHACUN DES 64 PIONS OUI JE SUIS FOU
        /*
                                for(int i=0; i<m_size; i++)
                                    for(int j=0; j<m_size; j++)
                                    {
                                        std::cout<<i<<" / "<<j<<std::endl;
                                        std::cout<<m_othellier[i][j].get_part_of_adjacence(1)<<m_othellier[i][j].get_part_of_adjacence(2)<<m_othellier[i][j].get_part_of_adjacence(3)<<std::endl;
                                        std::cout<<m_othellier[i][j].get_part_of_adjacence(0)<<" "<<m_othellier[i][j].get_part_of_adjacence(4)<<std::endl;
                                        std::cout<<m_othellier[i][j].get_part_of_adjacence(7)<<m_othellier[i][j].get_part_of_adjacence(6)<<m_othellier[i][j].get_part_of_adjacence(5)<<std::endl<<std::endl;
                                    }
        */
        ///Encore pire, pour tester, on affiche les matrices de changement de chaque possibilit�
        /*
                for(unsigned int p=0; p<m_possibilities.size(); p++)
                {
                    for(int i=0; i<m_size; i++)
                    {
                        for(int j=0; j<m_size; j++)
                        {
                            std::cout<<m_possibilities[p].get_part_of_change(j,i);
                        }
                        std::cout<<std::endl;
                    }
                    std::cout<<std::endl<<std::endl<<std::endl;
                }
        */
        if(m_console->getInputKey()=='z' && m_curseur_y>0)
            m_curseur_y+=-1;
        else if(m_console->getInputKey()=='s' && m_curseur_y<m_size-1)
            m_curseur_y+=1;
        else if(m_console->getInputKey()=='q' && m_curseur_x>0)
            m_curseur_x+=-1;
        else if(m_console->getInputKey()=='d' && m_curseur_x<m_size-1)
            m_curseur_x+=1;
        else if(m_console->getInputKey()==Enter_bouton)
        {
            valid_entry=true;
        }
        else if(m_console->getInputKey()==Echap_bouton)
        {
            m_Echap= true;
            m_valid_choice=true;
            if(m_beginner_AI)
                create_save(m_save_file_beginner_AI);
            else if(m_intermediary_AI)
                create_save(m_save_file_intermediary_AI);
            else
                create_save(m_save_file);
        }
        else
            fflush(stdin);
    }
    valid_entry=false;
}

void Othello::show_game()
{
    char mur=char(196);
    char mur_vertical=char(179);
    char mur_connecteur=char(197);
    char petit_pion=char(254);
    bool possible_move=false;///Va contenir l'information s'il est possible de jouer � une certaine case
    int dif_MAJ=65;///Position du A dans la table ASCII

    ///Positions de d�part, un peu choisies au pif
    int initial_x=25;
    int initial_y=7;

    int pos_x=initial_x;
    int pos_y=initial_y;

    ///Affichage des num�ros des cases
    for(int i=0; i<m_size; i++)
    {
        get_console()->gotoLigCol(pos_y,pos_x);
        std::cout<<"  "<<char(i+dif_MAJ);
        pos_x+=3;
    }
    pos_x=initial_x;
    pos_y+=2;

    get_console()->_setColor(COLOR_BLACK,COLOR_GREEN);

    for (int i=0; i<m_size; i++)
    {
        get_console()->gotoLigCol(pos_y,pos_x);

        std::cout<<mur_connecteur;
        for (int k=0; k<m_size; k++)
            std::cout <<mur<<mur<<mur_connecteur;

        pos_y+=1;
        pos_x-=3;///On se d�cale pour afficher la coordon�e lettre � gauche du tableau

        get_console()->_setColor(COLOR_WHITE,COLOR_BLACK);
        get_console()->gotoLigCol(pos_y,pos_x);

        std::cout<<i;
        pos_x+=3;///On se repositionne

        get_console()->gotoLigCol(pos_y,pos_x);
        get_console()->_setColor(COLOR_BLACK,COLOR_GREEN);

        for (int j=0; j<m_size; j++)
        {
            std::cout <<mur_vertical;
            if(m_othellier[i][j].get_side()==1)
                get_console()->_setColor(COLOR_WHITE,COLOR_GREEN);
            else if(m_othellier[i][j].get_side()==0)
                get_console()->_setColor(COLOR_BLACK,COLOR_GREEN);
            else
            {
                ///Affichage des cases sur lesquelles un coup peut �tre fait
                for(unsigned int p=0; p<m_possibilities.size(); p++)
                    if(i==m_possibilities[p].get_coord_y() && j==m_possibilities[p].get_coord_x())
                    {
                        get_console()->_setColor(COLOR_GREY,COLOR_GREEN);
                        possible_move=true;
                    }
            }

            if(m_othellier[i][j].get_side()!=-1 || possible_move)///On affiche un pion si la case est occup�e, du vide sinon
                std::cout<<petit_pion;
            else
                std::cout<<" ";

            std::cout<<" ";
            get_console()->_setColor(COLOR_BLACK,COLOR_GREEN);
            possible_move=false;///On indique bien que l'on a plus aucune id�e si l'on peut afficher quelque chose sur la case suivante
        }

        std::cout<<mur_vertical;
        pos_y+=1;
    }
    get_console()->gotoLigCol(pos_y,pos_x);

    ///Affichage de la partie basse du tableau
    std::cout<<mur_connecteur;
    for (int k=0; k<m_size; k++)
        std::cout <<mur<<mur<<mur_connecteur;

    ////Affichage du curseur
    get_console()->_setColor(COLOR_RED,COLOR_GREEN);
    get_console()->gotoLigCol(initial_y+m_curseur_y*2+2,initial_x+m_curseur_x*3);
    std::cout<<mur_connecteur<<mur<<mur<<mur_connecteur;

    get_console()->gotoLigCol(initial_y+m_curseur_y*2+3,initial_x+m_curseur_x*3);
    std::cout<<mur_vertical;
    get_console()->gotoLigCol(initial_y+m_curseur_y*2+3,initial_x+3+m_curseur_x*3);
    std::cout<<mur_vertical;

    get_console()->gotoLigCol(initial_y+m_curseur_y*2+4,initial_x+m_curseur_x*3);
    std::cout<<mur_connecteur<<mur<<mur<<mur_connecteur;
    ////

    get_console()->_setColor(COLOR_WHITE,COLOR_BLACK);
    get_console()->gotoLigCol(pos_y,pos_x);
    std::cout<<std::endl<<std::endl<<std::endl;

    std::cout<<m_nb_pions<<std::endl;///On affiche le nombre de pions pour les tests...

    if(!m_turn_white)
        std::cout<<"TOUR DES NOIRS"<<std::endl;
    else
        std::cout<<"TOUR DES BLANCS"<<std::endl;

    ///On efface l'affichage du tableau possible au tour suivant s'il y en  a un
    if(m_showing_copy)
    {
        get_console()->gotoLigCol(pos_y+15,pos_x-3);
        for(int y=0; y<25; y++)
        {
            std::cout<<"                                          ";
            get_console()->gotoLigCol(pos_y+15+y,pos_x-3);
        }
        m_showing_copy=false;
    }
    get_console()->gotoLigCol(pos_y+10,0);

    show_possibilities();
    show_tree(pos_x, pos_y+15);
}

void Othello::show_possibilities()
{
    if(m_beginner_AI && ((m_player_white && !m_turn_white) ||(!m_player_white && m_turn_white)))///Simplement: c'est � l'IA d�butante de jouer
        std::cout<<"Les coups pouvant etre fait par l'ordinateur debutant sont: "<<std::endl;
    else if(m_intermediary_AI && ((m_player_white && !m_turn_white) ||(!m_player_white && m_turn_white)))
        std::cout<<"Les coups pouvant etre fait par l'ordinateur intermediaire sont: "<<std::endl;
    else
        std::cout<<"Les coups que vous pouvez faire sont: "<<std::endl;

    for(unsigned int i=0; i<m_possibilities.size(); i++)
        std::cout<<m_possibilities[i].get_coord_y()<<char(m_possibilities[i].get_coord_x()+65)<<std::endl;
}

void Othello::show_tree(int position_x, int position_y)
{
    char mur=char(196);
    char mur_vertical=char(179);
    char mur_connecteur=char(197);
    char petit_pion=char(254);
    int dif_MAJ=65;///Position du A dans la table ASCII

    ///Positions de d�part, un peu choisies au pif
    int initial_x=position_x;
    int initial_y=position_y;

    int pos_x=initial_x;
    int pos_y=initial_y;
    unsigned int n=0;

    while (n<m_possibilities.size())
    {
        if(m_curseur_y==m_possibilities[n].get_coord_y() && m_curseur_x==m_possibilities[n].get_coord_x())
        {
            m_showing_copy=true;
            ///Affichage des num�ros des cases
            for(int i=0; i<m_size; i++)
            {
                get_console()->gotoLigCol(pos_y,pos_x);
                std::cout<<"  "<<char(i+dif_MAJ);
                pos_x+=3;
            }
            pos_x=initial_x;
            pos_y+=2;

            get_console()->_setColor(COLOR_BLACK,COLOR_GREEN);

            for (int i=0; i<m_size; i++)
            {
                get_console()->gotoLigCol(pos_y,pos_x);

                std::cout<<mur_connecteur;
                for (int k=0; k<m_size; k++)
                    std::cout <<mur<<mur<<mur_connecteur;

                pos_y+=1;
                pos_x-=3;///On se d�cale pour afficher la coordon�e lettre � gauche du tableau

                get_console()->_setColor(COLOR_WHITE,COLOR_BLACK);
                get_console()->gotoLigCol(pos_y,pos_x);

                std::cout<<i;
                pos_x+=3;///On se repositionne

                get_console()->gotoLigCol(pos_y,pos_x);
                get_console()->_setColor(COLOR_BLACK,COLOR_GREEN);

                for (int j=0; j<m_size; j++)
                {
                    std::cout <<mur_vertical;
                    if((m_othellier[i][j].get_side()==1 && !m_possibilities[n].get_part_of_change(j,i)) || (m_turn_white && m_possibilities[n].get_part_of_change(j,i)))
                        get_console()->_setColor(COLOR_WHITE,COLOR_GREEN);
                    else if((m_othellier[i][j].get_side()==0 && !m_possibilities[n].get_part_of_change(j,i)) || (!m_turn_white && m_possibilities[n].get_part_of_change(j,i)))
                        get_console()->_setColor(COLOR_BLACK,COLOR_GREEN);

                    if(m_othellier[i][j].get_side()!=-1 || m_possibilities[n].get_part_of_change(j,i))///On affiche un pion si la case est occup�e, du vide sinon
                        std::cout<<petit_pion;
                    else
                        std::cout<<" ";

                    std::cout<<" ";
                    get_console()->_setColor(COLOR_BLACK,COLOR_GREEN);
                }

                std::cout<<mur_vertical;
                pos_y+=1;
            }
            get_console()->gotoLigCol(pos_y,pos_x);

            ///Affichage de la partie basse du tableau
            std::cout<<mur_connecteur;
            for (int k=0; k<m_size; k++)
                std::cout <<mur<<mur<<mur_connecteur;

            n=m_possibilities.size();
        }
        n++;
    }
    get_console()->_setColor(COLOR_WHITE,COLOR_BLACK);

}

void Othello::turn()
{
    ///On d�termine les possibilit�s du tour
    determine_possibilities(m_othellier, m_possibilities);

    if(m_possibilities.size()==0 && m_no_possible_move)///Il ne reste plus aucune possibilit� de coup, alors on passe le tour
        end_game();

    ///Test de fin de partie
    if(m_possibilities.size()==0)//Il n'y a pas de possibilit� de coup pour ce tour, donc on le note et on verra si le prochain tour est impossible
        m_no_possible_move=true;
    else
    {
        m_no_possible_move=false;

        if(!m_Echap)///Du moment que l'on quitte pas le jeu, o� que celui-ci n'est pas fini
        {
            ///Action du joueur, ou de l'une des IA
            if(m_beginner_AI && ((m_player_white && !m_turn_white) ||(!m_player_white && m_turn_white)))///Test bien trop compliqu� pour savoir si on est au tour du joueur ou non
            {
                ///Cas IA debutante
                move_choice();
                beginner_AI();
            }
            else if(m_intermediary_AI && ((m_player_white && !m_turn_white) ||(!m_player_white && m_turn_white)))
            {
                ///Cas IA intermediaire
                move_choice();
                intermediary_AI(5);
            }
            else
            {
                ///Ici on est dans le cas du tour du joueur
                while(!m_valid_choice)
                {
                    move_choice();
                    for(unsigned int p=0; p<m_possibilities.size(); p++)
                        if(m_possibilities[p].get_coord_x()==m_curseur_x && m_possibilities[p].get_coord_y()==m_curseur_y)
                        {
                            apply_change(p, m_othellier, m_possibilities);
                            m_valid_choice=true;
                        }
                }
                m_valid_choice=false;
            }
        }
    }
    end_turn();
}

void Othello::determine_possibilities(std::vector<std::vector<Pion>> othellier, std::vector<Coup> &possibilities)
{
    ///L'ordre de cr�ation des variables est plus ou moins leur ordre d'utilisation dans le programme
    int pion_ennemy;
    int pion_ally;
    int emptyness=-1;

    ///Valeur des directions selon x et y
    int dir_x=0;
    int dir_y=0;

    int coord_x=0;
    int coord_y=0;

    int n=1;///Avancement dans la direction en question
    bool reached_ally=false;
    bool reached_emptyness=false;

    bool possible_move=false;
    unsigned int nb_possible_moves=0;

    pion_ennemy=int(!m_turn_white);
    pion_ally=int(m_turn_white);

    int p=possibilities.size();
    ///Vidage des possibilit�s qui seraient encore l� pour des raisons inconnues...
    for(int i=0; i<p; i++)
        possibilities.pop_back();

    ////Phase de d�termination des coups possibles pour ce tour, soit la quasi int�gralit� du jeu
    for(int i=0; i<m_size; i++)
    {
        for(int j=0; j<m_size; j++)
        {
            for(int k=0; k<8; k++)
            {
                if (othellier[i][j].get_part_of_adjacence(k)==pion_ennemy && (othellier[i][j].get_side()==emptyness || possible_move))
                {
                    ///On va ici d�terminer dans quelle direction va se faire les tests successifs dans le tableau
                    if(k==0)
                    {
                        dir_x=-1;
                        dir_y=0;
                    }
                    else if(k==1)
                    {
                        dir_x=-1;
                        dir_y=-1;
                    }
                    else if(k==2)
                    {
                        dir_x=0;
                        dir_y=-1;
                    }
                    else if(k==3)
                    {
                        dir_x=1;
                        dir_y=-1;
                    }
                    else if(k==4)
                    {
                        dir_x=1;
                        dir_y=0;
                    }
                    else if(k==5)
                    {
                        dir_x=1;
                        dir_y=1;
                    }
                    else if(k==6)
                    {
                        dir_x=0;
                        dir_y=1;
                    }
                    else if(k==7)
                    {
                        dir_x=-1;
                        dir_y=1;
                    }

                    while(!reached_ally && !reached_emptyness && i+(n+1)*dir_y>=0 && j+(n+1)*dir_x>=0 && i+(n+1)*dir_y<=m_size-1 && j+(n+1)*dir_x<=m_size-1)///Tant qu'on a pas touch� un alli� ou le vide et qu'on risque pas de sortir
                    {
                        n+=1;
                        coord_x=j+n*dir_x;
                        coord_y=i+n*dir_y;

                        if(othellier[coord_y][coord_x].get_side()==pion_ally)
                        {
                            reached_ally=true;
                            if(!possible_move)
                            {
                                nb_possible_moves++;///On indique au jeu qu'il y a un coup en plus de valable
                                possible_move=true;///On bloque l'incr�mentation du nombre de coup, on va pas compter 8 fois le m�me coup juste car il a un effet dans les 8 directions
                            }
                            /*
                            std::cout<<j<<"///"<<i<<std::endl;
                            std::cout<<n<<std::endl;
                            std::cout<<dir_x<<"///"<<dir_y<<std::endl;
                            std::cout<<j+n*dir_x<<"///"<<i+n*dir_y<<std::endl;
                            */
                        }
                        else if(othellier[coord_y][coord_x].get_side()==emptyness)
                            reached_emptyness=true;
                    }

                    if(reached_ally && possibilities.size()<nb_possible_moves)
                        possibilities.push_back(Coup(j,i,dir_x,dir_y,n));///On cr�er une case si c'est la premi�re modification du coup
                    else if(reached_ally)
                        possibilities[nb_possible_moves-1].update_possibilities(dir_x,dir_y,n);///Autrement on met simplement � jour
                }
                n=1;
                reached_ally=false;
                reached_emptyness=false;
            }
            possible_move=false;
        }
    }
}

void Othello::apply_change(unsigned int position, std::vector<std::vector<Pion>> &othellier, std::vector<Coup> possibilities)
{
    for(int i=0; i<m_size; i++)
        for(int j=0; j<m_size; j++)
            if(possibilities[position].get_part_of_change(j,i))
            {
                othellier[i][j].set_side(int(m_turn_white));
                set_adjacence(j,i,int(m_turn_white));
            }
}

void Othello::end_turn()
{
    int n;
    n=m_possibilities.size();

    for(int i=0; i<n; i++)///On vide le cache des possibilit�s
        m_possibilities.pop_back();

    ///On remet l'othello en ordre en donnant l'information des pions adjacents � chaque pion de l'othello
    for(int i=0; i<m_size; i++)
        for(int j=0; j<m_size; j++)
            set_adjacence(j,i,m_othellier[i][j].get_side());

    m_turn_white=true xor m_turn_white;
    m_nb_pions++;
    system("cls");
}

void Othello::end_game()
{
    int nb_white=0;
    int nb_black=0;

    for(int i=0; i<m_size; i++)
        for(int j=0; j<m_size; j++)
        {
            if (m_othellier[i][j].get_side()==0)
                nb_black++;
            else if(m_othellier[i][j].get_side()==1)
                nb_white++;
        }

    ///Je sais pas trop si une �galit� est possible dans une partie d'Othello mais dans le doute on le code, �a nous coute rien
    if(nb_white>nb_black)
        std::cout<<"Les blancs sont vainqueurs!"<<std::endl;
    else if(nb_black>nb_white)
        std::cout<<"Les noirs sont vainqueurs!"<<std::endl;
    else if(nb_black==nb_white)
        std::cout<<"Aucun n'est vainqueur, mais chacun repart avec son honneur!"<<std::endl;

    std::cout<<"SCORES:"<<std::endl<<"Blanc: "<<nb_white<<std::endl<<"Noir: "<<nb_black<<std::endl;

    system("pause");
    m_Echap=true;

    clear_game();
}

void Othello::clear_game()
{
    int n;
    m_nb_pions=0;
    m_curseur_x=0;
    m_curseur_y=0;
    m_showing_copy=false;

    m_turn_white=false;
    m_player_white=false;
    m_valid_choice=false;
    m_no_possible_move=false;
    m_beginner_AI=false;
    m_intermediary_AI=false;

    ////Vidage des possibilit�s
    n=m_possibilities.size();

    for(int i=0; i<n; i++)
        m_possibilities.pop_back();

    ////Vidage de l'othellier
    for(int i=0; i<m_size; i++)
        for(int j=0; j<m_size; j++)
        {
            m_othellier[i][j].set_side(-1);
            set_adjacence(j,i,-1);
        }
}

void Othello::game()
{
    system("cls");
    while(!m_Echap)
    {
        turn();
    }
    clear_game();
    m_Echap=false;
}

void Othello::beginner_AI()
{
    ///IA qui choisit au pif parmis les possibilit�s
    unsigned int random_choice;

    random_choice=rand()%m_possibilities.size();
    apply_change(random_choice, m_othellier, m_possibilities);
}

void Othello::intermediary_AI(int depth)
{
    std::vector<int> chosen_possibility;
    int rank_of_chosen_possibility=0;

    chosen_possibility=Min_Max(m_othellier, depth);

    for(unsigned int p=0; p<m_possibilities.size(); p++)
        if(m_possibilities[p].get_coord_x() == chosen_possibility[0] && m_possibilities[p].get_coord_y() == chosen_possibility[1])
            rank_of_chosen_possibility=p;

    apply_change(rank_of_chosen_possibility, m_othellier, m_possibilities);
}

std::vector<int> Othello::Min_Max(std::vector<std::vector<Pion>> othellier, int depth)
{
    ///A noter qu'ici on n'utilise pas les r�f�rences, othellier et possibilities sont donc des copies des tableaux donn�s � la fonction
    ///Les changer n'aura aucune cons�quence sur les donn�es de la fonction appellante (en th�orie hein)

    int nb_possibilities;///Variable qui va contenir la taille du tableau de possibilit�s de coups
    int chosen_weight=-100;///Poids du tour actuel
    int chosen_x;7
    int chosen_y;

    std::vector<int> weights;
    std::vector<int> info_to_return;///Ceci va contenir la coordon�e x, la coordon�e y, et enfin le poids pond�r� (en prenant en compte les coups suivants) du coup que l'on choisit d'effectuer
    std::vector<std::vector<int>> info_from_deeper_lever;
    std::vector<Coup> possibilities;

    determine_possibilities(othellier, possibilities);
    nb_possibilities=possibilities.size();

    for(int p=0; p<nb_possibilities; p++)
    {
        std::vector<std::vector<Pion>> copie_othellier=othellier;

        ///D�termination du poids du coup
        possibilities[p].set_weight();
        weights.push_back(possibilities[p].get_weight());

        apply_change(p, copie_othellier, possibilities);

        if(depth!=1)
            info_from_deeper_lever.push_back(Min_Max(copie_othellier, depth-1));
        else if(weights[p]>chosen_weight)
            chosen_weight=weights[p];

        if(depth!=1 && weights[p]-info_from_deeper_lever[p][2]>chosen_weight)
        {
            chosen_weight=weights[p]-info_from_deeper_lever[p][2];
            chosen_x=possibilities[p].get_coord_x();
            chosen_y=possibilities[p].get_coord_y();
        }
    }

    info_to_return.push_back(chosen_x);
    info_to_return.push_back(chosen_y);
    info_to_return.push_back(chosen_weight);

    return info_to_return;
}

void Othello::set_adjacence(int coord_x, int coord_y, int side)
{
    ///On est parti pour indiquer � chaque pion quels sont les pions qui lui sont adjacents
    ///(oui on va �tre les seuls � faire un truc aussi malsain d'esprit mais c'est pas grave on est des g�nies)
    ///On fait les blindages n�cessaires pour pas appeller set_adjacence_pions en dehors du tableau

    if(coord_x>0)
    {
        m_othellier[coord_y][coord_x-1].set_adjacence_pions(side,4);
        if(coord_y>0)
            m_othellier[coord_y-1][coord_x-1].set_adjacence_pions(side,5);
        if(coord_y<m_size-1)
            m_othellier[coord_y+1][coord_x-1].set_adjacence_pions(side,3);
    }
    if(coord_x<m_size-1)
    {
        m_othellier[coord_y][coord_x+1].set_adjacence_pions(side,0);
        if(coord_y>0)
            m_othellier[coord_y-1][coord_x+1].set_adjacence_pions(side,7);
        if(coord_y<m_size-1)
            m_othellier[coord_y+1][coord_x+1].set_adjacence_pions(side,1);
    }
    if(coord_y>0)
        m_othellier[coord_y-1][coord_x].set_adjacence_pions(side,6);
    if(coord_y<m_size-1)
        m_othellier[coord_y+1][coord_x].set_adjacence_pions(side,2);
}
