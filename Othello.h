#include <iostream>
#include <fstream>
#include <vector>
#include <windows.h>

#include "console.h"
#include "Pion.h"
#include "Coup.h"

class Othello
{
private:
    std::vector<std::vector<Pion>> m_othellier;
    std::vector<Coup> m_possibilities;
    Console* m_console=NULL;

    int m_nb_pions=0;
    int m_size=8;
    int m_curseur_x=0;
    int m_curseur_y=0;

    int m_previous_x=0;
    int m_previous_y=0;
    bool m_showing_copy=false;///Indique si l'on est en train d'afficher le r�sultat d'un des coups possibles

    bool m_turn_white=false;/// true-> tour des blancs, false-> tour des noirs
    bool m_player_white=false;///Indique si le joueur (plutot que l'IA) joue les blancs
    bool m_simulating_IA_turn=false;///Indique si l'IA est en train d'op�rer Min_Max sur un tour � lui ou sur un tour adverse

    bool m_no_possible_move=false;
    bool m_Echap=false;///true lorsque l'on sort du jeu, permet de sauter des parties de programmes
    bool m_valid_choice=false;///true lorsque le joueur appuie sur entr�e

    ///IA actives ou non
    bool m_beginner_AI=false;
    bool m_intermediary_AI=false;

    std::string m_save_file="Save_1vs1.txt";
    std::string m_save_file_beginner_AI="Save_vs_ordi_debutant.txt";
    std::string m_save_file_intermediary_AI="Save_vs_ordi_intermediaire.txt";

public:
    Othello();
    ~Othello();
    Console* get_console();

    void initiate_othellier();
    void launch_menu();
    void side_choice();

    ///Fonctions de calcul et d�termination de l'�tat du jeu effectu�es � chaque tour
    void move_choice();
    void determine_possibilities(std::vector<std::vector<Pion>> othellier, std::vector<Coup> &possibilities);
    void apply_change(unsigned int position, std::vector<std::vector<Pion>> &othellier,std::vector<Coup> m_possibilities);
    void turn();
    void end_turn();

    ///Fonctions de boucle de jeu, remise � z�ro du jeu
    void end_game();
    void clear_game();
    void game();

    ///Fonctions de gestion des IA
    void beginner_AI();
    void intermediary_AI(int depth);
    std::vector<int> Min_Max(std::vector<std::vector<Pion>> othellier, int depth);

    ///Fonctions d'affichage
    void show_game();
    void show_possibilities();
    void show_tree(int position_x, int postion_y);

    ///Fonctions de gestion des fichiers
    void get_save(std::string file_name);
    void create_save(std::string file_name);

    void set_adjacence(int coord_x, int coord_y, int side);

};
