#include "Pion.h"

Pion::Pion()
{
    for(unsigned int i=0; i<8; i++)
        m_adjacence_pions.push_back(-1);
}

Pion::Pion(int coord_x, int coord_y, int side)
    :m_coord_x(coord_x), m_coord_y(coord_y), m_side(side)
{
    for(unsigned int i=0; i<8; i++)
        m_adjacence_pions.push_back(-1);
}

Pion::~Pion()
{

}

int Pion::get_coord_x()
{
    return m_coord_x;
}

int Pion::get_coord_y()
{
    return m_coord_y;
}

int Pion::get_side()
{
    return m_side;
}

void Pion::set_side(int side)
{
    m_side=side;
}

void Pion::set_adjacence_pions(int side, int direction)
{
    m_adjacence_pions[direction]=side;
}

int Pion::get_part_of_adjacence(int direction)
{
    return m_adjacence_pions[direction];
}
