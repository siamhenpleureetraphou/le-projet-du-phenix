#include <vector>

class Pion
{
private:
    ///Sans doute peut-�tre pas si useless, enfin on verra plus tard
    int m_coord_x;
    int m_coord_y;

    ///Alors maintenant on part dans le turfu, on va faire un tableau de 8 cases qui dit pour chacune des 8 cases autour si elle est occup�e ou non, et par quoi tant qu'� faire
    std::vector<int> m_adjacence_pions;

   int m_side;/// � -1 si la case est vide, 0 si noir, 1 si blanc

public:
    Pion();
    Pion(int coord_x, int coord_y, int side);
    ~Pion();

    int get_coord_x();
    int get_coord_y();
    int get_side();

    void set_side(int side);
    void set_adjacence_pions(int side, int direction);

    int get_part_of_adjacence(int direction);
};
