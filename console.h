#ifndef CONSOLE_H_INCLUDED
#define CONSOLE_H_INCLUDED

#include <iostream>

enum Color
{
    COLOR_BLACK=0,
    COLOR_DARK_BLUE=1,
    COLOR_GREEN=2,
    COLOR_GREY_BLUE=3,
    COLOR_BROWN=4,
    COLOR_PURPLE=5,
    COLOR_KAKI=6,
    COLOR_LIGHT_GREY=7, // gris couleur de base de la console
    COLOR_GREY=8,
    COLOR_BLUE=9,
    COLOR_LIGHT_GREEN=10,
    COLOR_TURQUOISE=11,
    COLOR_RED=12,
    COLOR_PINK=13,
    COLOR_YELLOW=14,
    COLOR_WHITE=15,
};

/*
0: noir
1: bleu fonc�
2: vert
3: bleu-gris
4: marron
5: violet
6: kaki
7: gris clair
8: gris
9: bleu
10: vert fluo
11: turquoise
12: rouge
13: rose fluo
14: jaune fluo
15: blanc
*/

class Console
{
    private:
        // Empecher la cr�ation
        Console();
        ~Console();

        // Empecher la copie d'objet...
        Console& operator= (const Console&){ return *this;}
        Console (const Console&){}

        // Attributs
        static Console* m_instance;

    public:
        void _setColor(int front, int back);
        // M�thodes statiques (publiques)
        static Console* getInstance();
        static void deleteInstance();

        // M�thodes publiques
        void gotoLigCol(int lig, int col);
        bool isKeyboardPressed();
        int getInputKey();
        void setColor(Color col);
};

#endif // CONSOLE_H_INCLUDED
